
import React, { Component } from 'react';

/**
 * validation component validates length of content.
 * Displays 'text long enough' if given lenght is greater than else 'Text too Short'
 * 
 * @param {*} props properties received
 */
class WordValidator extends Component {

  render() {
    console.log('[WordValidator.js] : render');
    const message = (this.props.contentLength > 5) ? 'Text long enough' : 'Text too Short';
    return (<div><p> {message} </p></div>);
  }
};

export default WordValidator;
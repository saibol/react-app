import React, { Component } from 'react';
import Character from '../Character/Character';

class Characters extends Component {
    render() {
        return this.props.content.split('')
            .map((c, index) =>
                <Character
                    charClicked={this.props.charRemoved}
                    charIndex={index}
                    key={index}
                    char={c} />);
    }
};
export default Characters;


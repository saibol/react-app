import React, { Component } from 'react';
import classes from './Character.css'

/**
 * displays character with provided style i.e. displays in box 
 * and call charClicked function upon clicking on box
 * 
 */
class Character extends Component{
    render (){
        return (<div className={classes.boxStyle}
            onClick={() => this.props.charClicked(this.props.charIndex)}>
            {this.props.char}
        </div>);
    }
};

export default Character;
import React, { Component } from 'react';
import classes from './App.css';
import WordValidator from '../components/Validation/WordValidator';
import Characters from '../components/Characters/Characters';

class App extends Component {
  
  constructor(props){
    console.log('[App.js] : constructor');
    super(props);
    this.state = {
      content: ''
    }
  }

  static getDerivedStateFromProps(props, state){
    console.log('[App.js] : getDerivedStateFromProps');
    return state;
  }

  componentDidMount(){
    console.log('[App.js] : componentDidMount');
  }

  // set content with updated value
  contentReloadHandler = (event) => {
    this.setState({
      content: event.target.value
    });
  };

  // remove char from content for given index
  removeCharHandler = (index) => {
    let data = [...this.state.content];
    data.splice(index, 1);
    this.setState({
      content: data.join('')
    });
  };

  render() {
    console.log('[App.js] : render');

    return (
      <div>
        <p className={classes.intro} >Hello World..!!</p><br/>
        Enter content : <input type="text" onChange={this.contentReloadHandler}></input><br />
        <p>{this.state.content}</p>
        <WordValidator
          contentLength={this.state.content.length} />
        <Characters
          content={this.state.content}
          charRemoved={this.removeCharHandler } />
      </div>
    );
  }
}

export default App;
